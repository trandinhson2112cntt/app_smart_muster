export interface IRegisterParam { 
    maso: string,
    matkhau: string,
    hoten: string,
    diachi: string,
    dienthoai: string,
    email: string,
    typeUser: string,
}
export class RegisterParam implements IRegisterParam { 
    maso: string;
    matkhau: string;
    hoten: string;
    diachi: string;
    dienthoai: string;
    email: string;
    typeUser: string;
    
    constructor(opt: IRegisterParam) {
        this.maso = opt.maso;
        this.matkhau = opt.matkhau;
        this.hoten = opt.hoten;
        this.diachi = opt.diachi;
        this.dienthoai = opt.dienthoai;
        this.email = opt.email;
        this.typeUser = opt.typeUser;
    }
}