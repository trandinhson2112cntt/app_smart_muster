import { IsNotEmpty, IsNumber, validate, IsString, MinLength, IsPhoneNumber, IsEmail } from "class-validator";

export interface IRegisterModel {
    code: string,
    password: string,
    name: string,
    address: string,
    phone: string,
    email: string,
    typeUser: string,
}
export class RegisterModel implements IRegisterModel {
    @IsNotEmpty({ message: "Mã số không được bỏ trống!" })
    code: string;

    @IsNotEmpty({ message: "Mật khẩu không được bỏ trống!" })
    password: string;

    @MinLength(6, {message : "Tên phải ít nhất 6 ký tự"})
    name: string;
    @IsNotEmpty({message:"Địa chỉ không được bỏ trống!"})
    address: string;
    @MinLength(10,{message:"Điện thoại ít nhất 10 số!"})
    phone: string;
    
    @IsNotEmpty({message:"Email không được bỏ trống!"})
    @IsEmail()
    email: string;
    typeUser: string;

    constructor(opt: IRegisterModel) {
        this.code = opt.code;
        this.password = opt.password;
        this.name = opt.name;
        this.address = opt.address;
        this.phone = opt.phone;
        this.email = opt.email;
        this.typeUser = opt.typeUser;
    }

    static validateData(item: IRegisterModel){
        return(
            validate(item)
                .then((err:any) => {
                    let str:string = "";
                    err.map((item:any) => Object.values(item.constraints).map((item:any) => str += item + "\n"));
                    return str;
                })
        )
    }

}