
import { IsNotEmpty, MinLength } from "class-validator"
export interface ILoginDataResponse {
    status: number,
    message: string,
    data?: {}
}
export class LoginDataResponse implements ILoginDataResponse {

    status: number;
    message: string;
    data?: {}
    constructor(opt: ILoginDataResponse) {
        this.status = opt.status;
        this.message = opt.message;
        this.data = opt.data;
    }

    
}