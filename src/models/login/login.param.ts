
import { IsNotEmpty, MinLength, validate } from "class-validator"
export interface ILoginParam {
    code: string,
    password: string
}
export class LoginParam implements ILoginParam {

    @IsNotEmpty({ message: "Mã số không được bỏ trống"})
    code: string;

    @IsNotEmpty({ message: "Mật khẩu không được bỏ trống"})
    @MinLength(6,{ message: "Mật khẩu phải có hơn 6 ký tự"})
    password: string;

    constructor(opt: ILoginParam) {
        this.code = opt.code;
        this.password = opt.password;
    }

    static validateData(item: ILoginParam){
        return(
            validate(item)
                .then((err:any) => {
                    let str:string = "";
                    err.map((item:any) => Object.values(item.constraints).map((item:any) => str += item + "\n"));
                    return str;
                })
        )
    }

}