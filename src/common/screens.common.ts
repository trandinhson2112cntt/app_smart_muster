export default {
  Home: "Home",
  Schedule: "Schedule",
  Study: "Study",
  Others: "Others",
  Login: "Login",
  Register: "Register",
  ForgotPassword: "ForgotPassword",
  Notification:"Notification"
}