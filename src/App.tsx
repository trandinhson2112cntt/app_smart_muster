/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * 
 * Generated with the TypeScript template
 * https://github.com/emin93/react-native-template-typescript
 * 
 * @format
 */
import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import './plugins/axios';
import createStore from './store';
import { Provider } from 'react-redux';
import AppNavigator from "./navigation";
import { NavigationService } from './services';

interface Props { }

export default class App extends Component<Props> {

  store: any
  /**
   *
   */
  constructor(props: Readonly<Props>) {
    super(props);
    this.store = createStore()
  }

  render() {
    return (
      <Provider store={this.store} >
        <AppNavigator ref={(navigationRef: any) => NavigationService.setTopLevelNavigator(navigationRef)} />
      </Provider>
    );
  }
}
