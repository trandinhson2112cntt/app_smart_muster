import { StyleSheet } from "react-native";
import { default as colors } from "./colors";


export const aStyles = StyleSheet.create({
  dFlex: {
    flex: 1,
    backgroundColor: colors.White
  },
  fontText: {
    fontFamily:'OpenSans-Regular'
  },
})

export { colors } 
