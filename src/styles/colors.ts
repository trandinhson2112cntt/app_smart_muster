import { StyleSheet } from "react-native";

export default {
  White: 'white',
  Black: '#131313',
  Gray: '#d4d4d4',
  Red: '#ff3737',
  HeaderColor: '#72D3F9',
  IconTabColor: '#72D3F9',
  IconTabColorInactive: '#848484'
}