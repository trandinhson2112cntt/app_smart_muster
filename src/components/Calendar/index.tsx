import React, { Component } from 'react';
import { Props, State } from './state-props';
import {
  Text,
  StyleSheet,
  ScrollView,
  View
} from 'react-native';
import { Calendar } from 'react-native-calendars';
//import styles from "./styles";

class Calendars extends React.Component<Props, State>{

  /**
   *
   */
  constructor(props: Readonly<Props>) {
    super(props);
    this.state = {};
    this.onDayPress = this.onDayPress.bind(this);
  }

  render() {
    return (
      <View style={styles.container}>
        <Calendar
          style={styles.calendar}
          onDayPress={(day) => {console.log('selected day', day)}}
          current={'2012-05-16'}
          minDate={'2012-05-02'}
          maxDate={'2012-05-30'}
          // Collection of dates that have to be marked. Default = {}
          markedDates={{
            '2012-05-16': {selected: true, marked: true, selectedColor: '#92DF4E'},
            '2012-05-07': {marked: true}, //những ngày có lịch dạy
            '2012-04-30': {marked: true}, //những ngày có lịch dạy
            '2012-05-14': {marked: true}, //những ngày có lịch dạy
            '2012-05-21': {marked: true}, //những ngày có lịch dạy
            '2012-05-28': {marked: true}, //những ngày có lịch dạy
            '2012-05-02': {marked: true}, //những ngày có lịch dạy
            '2012-05-09': {marked: true}, //những ngày có lịch dạy
            '2012-05-23': {marked: true}, //những ngày có lịch dạy
            '2012-05-30': {marked: true}, //những ngày có lịch dạy
            '2012-05-04': {marked: true}, //những ngày có lịch dạy
            '2012-05-11': {marked: true}, //những ngày có lịch dạy
            '2012-05-25': {marked: true}, //những ngày có lịch dạy
            '2012-05-18': {marked: true, dotColor: 'red', activeOpacity: 0}, //những ngày cho kiểm tra
          }}
        />
      </View>
    )
  }

  onDayPress(day: { dateString: any; }) {
    this.setState({
      selected: day.dateString
    });
  }
}

export default Calendars;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'gray'
  },
  calendar: {
    borderTopWidth: 1,
    paddingTop: 5,
    borderBottomWidth: 1,
    borderColor: '#E8E8E8',
    height: 400
  },
});