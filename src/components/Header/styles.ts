import { StyleSheet } from "react-native";
import { Values } from "../../common";
import { colors } from "../../styles";

export default StyleSheet.create({
  containerStyle: {
    height: 70,
    width: Values.FullWidth,
    backgroundColor: colors.HeaderColor,
    flexDirection: 'row',
  },
  iconStyle: {
    color: colors.White,
    textAlign:'center',
    justifyContent: 'center',
    fontSize: 24
  },
  iconCalendarStyle: {
    color: colors.White,
    textAlign:'center',
    justifyContent: 'center',
    fontSize: 34
  },
  headerLeft: {
    flex: 1,
    justifyContent: 'center',
    
  },
  title:{
    textAlign:'center',
    fontSize: 24,
    color: colors.White,
  },
  headerCenter:{
    flex: 4,
    justifyContent: 'center',
    alignItems: 'center',
  },
  
})