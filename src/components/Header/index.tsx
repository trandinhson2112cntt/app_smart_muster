import React, { Component } from 'react';
import { Props, State } from './state-props';
import { View, Text, TouchableOpacity } from 'react-native';
import styles from "./styles";
import { Icon, Picker } from 'native-base';
import { aStyles } from '../../styles';
import { NavigationService } from '../../services';

class Header extends React.Component<Props, State>{

  /**
   *
   */
  constructor(props: Readonly<Props>) {
    super(props);

  }

  

  render() {
    return (
      <View style={styles.containerStyle}>
        {this._renderIconBack()}
        {this._renderNULL()}
        <View style={styles.headerCenter}>
          <Text style={[styles.title,aStyles.fontText]}>{this.props.title}</Text>
        </View>
        {this._renderIconCalendar()}
        {this._renderIconNotification()}
      </View>
    )
  }
  _renderNULL(){
    if(this.props.isNull)
      return (
        <TouchableOpacity 
          style={styles.headerLeft}
          onPress={()=>{NavigationService.back()}}
        >
          {/*  */}
        </TouchableOpacity>
      )
    else  
        return (
          <View style={styles.headerLeft}></View>
        )
  }
  _renderIconBack(){
    if(this.props.isBack)
      return (
        <TouchableOpacity 
          style={styles.headerLeft}
          onPress={()=>{NavigationService.back()}}
        >
          <Icon
            name='ios-arrow-back'
            type='Ionicons'
            style={styles.iconStyle}
          />
        </TouchableOpacity>
      )
    else  
        return (
          <View style={styles.headerLeft}></View>
        )
  }

  _renderIconCalendar(){
    if(this.props.isCalendar)
      return (
        <TouchableOpacity 
          style={styles.headerLeft} 
          onPress={()=>{NavigationService.navigate('Calendar')}} 
        >
          <View>
            <Icon name="calendar" type='EvilIcons' style={styles.iconCalendarStyle} />
          </View>
        </TouchableOpacity>
      )
    else  
        return (
          <View style={styles.headerLeft}></View>
        )
  }

  _renderIconNotification(){
    if(this.props.isNotification)
      return (
        <TouchableOpacity 
          onPress={()=>{NavigationService.navigate('Notification')}}
          style={styles.headerLeft}
        >
          <Icon 
            name="bell-o" 
            type='FontAwesome'
            style={styles.iconStyle} 
          />
        </TouchableOpacity>
      )
    else  
        return (
          <View style={styles.headerLeft}></View>
        )
  }
}

export default Header;