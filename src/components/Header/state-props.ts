export interface Props { 
    title: string,
    isBack: boolean,
    isNotification: boolean,
    isCalendar: boolean,
    isNull: boolean,
}
export interface State { }