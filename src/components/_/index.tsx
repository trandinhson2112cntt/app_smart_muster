import React, { Component } from 'react';
import { Props, State } from './state-props';
import { View, Text } from 'react-native';
import styles from "./styles";

class BoilerPlate extends React.Component<Props, State>{

  /**
   *
   */
  constructor(props: Readonly<Props>) {
    super(props);
  }
  render() {
    return (
      <View
        style={styles.containerStyle}
      >
        <Text>hello boilerplate</Text>
      </View>
    )
  }
}

export default BoilerPlate