import React, { Component } from 'react';
import { Props, State } from './state-props';
import { View, Text, TouchableOpacity } from 'react-native';
import styles from "./styles";
import { Icon } from 'native-base';
import { aStyles } from '../../styles';
import { NavigationService } from '../../services';

class IconTabNav extends React.Component<Props, State>{

  /**
   *
   */
  constructor(props: Readonly<Props>) {
    super(props);

  }
  render() {
    return (
      <View>
        <Icon
              name={this.props.name}
              type='Ionicons'
              style={styles.iconStyle}
        />
      </View>
    )
  }
}

export default IconTabNav;