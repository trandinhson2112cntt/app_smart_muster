import { StyleSheet } from "react-native";
import { Values } from "../../common";
import { colors } from "../../styles";

export default StyleSheet.create({
  iconStyle: {
    color: colors.White,
    textAlign:'center',
    justifyContent: 'center',
    fontSize: 34
  },
})