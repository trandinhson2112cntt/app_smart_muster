import { ActivityStatus } from "../common";

export class RootMutation {
  setLoadingStatus<T>(state: T, key: string) {

    //@ts-ignore
    const target: any = state[key]
    target.activityStatus = ActivityStatus.Loading

    return {
      //@ts-ignore
      ...state,
      [key]: target
    }
  }
}