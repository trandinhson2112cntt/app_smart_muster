import { combineReducers } from "redux";


import { IScheduleState } from "./schedule";
import scheduleReducers from "./schedule/reducers";
import registerReducer from './register/reducers';
import loginReducer from "./login/reducers";
import forgotPasswordReducer from "./forgotPassword/reducers";
import { ILoginState } from "./login";
import { IForgotPasswordState } from "./forgotPassword";
import { IRegisterState } from "./register";

export interface IRootReducer {
  scheduleReducer: IScheduleState,
  loginReducer: ILoginState,
  forgotPasswordReducer: IForgotPasswordState,
  registerReducer:IRegisterState,
}

export default combineReducers<IRootReducer>({
  scheduleReducer: scheduleReducers,
  loginReducer: loginReducer,
  forgotPasswordReducer: forgotPasswordReducer,
  registerReducer:registerReducer,
})