import { RootActions, IActionTyped } from "../actions";
import { IScheduleParam } from "../../models/schedule";
import _ from "./types";

class ScheduleAction extends RootActions {

  getSchedules(param: IScheduleParam): IActionTyped<IScheduleParam> {
    return { type: _.GET_SCHEDULE, payload: param }
  }

}

export default new ScheduleAction()