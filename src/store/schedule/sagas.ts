import { IActionTyped } from "../actions";
import { takeLatest, put } from "@redux-saga/core/effects";
import _ from "./types";
import actions from "./actions";

function* getSchedules(action: IActionTyped) {
  try {
    yield put(actions.setLoading(_.SET_ACTIVITY_STATUS))
    
  } catch (error) {
    yield put(actions.setError(_.GET_SCHEDULE_FAILED, error))
  }
}

export function* watchGetSchedule() {
  yield takeLatest(_.GET_SCHEDULE, getSchedules)
}