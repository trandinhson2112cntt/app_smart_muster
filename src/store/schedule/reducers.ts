import { IScheduleState, ScheduleState } from "./state";
import { IActionTyped } from "../actions";
import _ from "./types";
import mutation from "./mutations";

enum ScheduleKeys {
  listSchedules = "scheduleStore"
}
const initialState: IScheduleState = new ScheduleState()

export default (state: IScheduleState = initialState, action: IActionTyped) => {

  switch (action.type) {

    default:
      return state
  }

}