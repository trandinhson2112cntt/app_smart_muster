import { all, fork } from "@redux-saga/core/effects";
//import { watchForgotPassword, watchLogin } from "./auth/sagas";
import { watchRegister } from "./register/sagas";
import { watchLogin } from "./login/sagas";
import { watchForgotPassword } from "./forgotPassword/sagas";
export function* rootSaga() {
  yield all([
    watchLogin(),
    watchForgotPassword(),
    watchLogin(),
    watchRegister()
  ])
}