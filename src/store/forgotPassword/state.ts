import { ActivityStatus } from "../../common";

export interface IForgotPasswordState {
  forgotPayload: {
    activityStatus: ActivityStatus,
    payload: any,
    error: any | null
  }
}

export class ForgotPasswordState implements IForgotPasswordState {
  forgotPayload: {
    activityStatus: ActivityStatus,
    payload: any,
    error: any | null
  }
  /**
   *
   */
  constructor() {
    this.forgotPayload = {
      activityStatus: ActivityStatus.NoActivity,
      error: null,
      payload: {}
    }
  }
}