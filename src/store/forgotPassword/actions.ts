import { RootActions, IActionTyped } from "../actions";
import { IScheduleParam } from "../../models/schedule";
import _ from "./types";

class ForgotPasswordAction extends RootActions {

  forgotPassword(emai: string): IActionTyped<string> {
    return { type: _.FORGOT_PASSWORD, payload: emai }
  }

}

export default new ForgotPasswordAction()