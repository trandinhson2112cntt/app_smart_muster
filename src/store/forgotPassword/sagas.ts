import { IActionTyped } from "../actions";
import { takeLatest, put } from "@redux-saga/core/effects";
import _ from "./types";
import actions from "./actions";
import { ForgotPasswordService, IForgotPasswordService } from "../../services";

const service: IForgotPasswordService = new ForgotPasswordService()

function* forgotPassword(action: IActionTyped<string>) {
  try {
    yield put(actions.setLoading(_.SET_ACTIVITY_STATUS))
    const result = yield service.fotgotPassword(action.payload!)
    yield put({ type: _.FORGOT_PASSWORD_SUCCESS, payload: result })
  } catch (error) {
    yield put(actions.setError(_.FORGOT_PASSWORD_FAILED, error))
  }
}

export function* watchForgotPassword() {
  yield takeLatest(_.FORGOT_PASSWORD, forgotPassword)
}

// function* login(action: IActionTyped<string>) {
//   try {
//     yield put(actions.setLoading(_.SET_ACTIVITY_STATUS))
//     const result = yield service.fotgotPassword(action.payload!)
//     yield put({ type: _.FORGOT_PASSWORD_SUCCESS, payload: result })
//   } catch (error) {
//     yield put(actions.setError(_.FORGOT_PASSWORD_FAILED, error))
//   }
// }

// export function* watchLogin() {
//   yield takeLatest(_.FORGOT_PASSWORD, login)
// }