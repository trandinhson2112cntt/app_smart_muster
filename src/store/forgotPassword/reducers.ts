import { IForgotPasswordState, ForgotPasswordState } from "./state";
import { IActionTyped } from "../actions";
import _ from "./types";
import mutation from "./mutations";

enum ScheduleKeys {
  listSchedules = "scheduleStore"
}
const initialState: IForgotPasswordState = new ForgotPasswordState()

export default (state: IForgotPasswordState = initialState, action: IActionTyped) => {

  switch (action.type) {
    case _.SET_ACTIVITY_STATUS:
      return mutation.setLoading(state, 'forgotPayload')
    case _.FORGOT_PASSWORD_SUCCESS:
      return mutation.setForgotSucess(state, action.payload)
    case _.FORGOT_PASSWORD_FAILED:
      return state
    default:
      return state
  }

}