import { RootActions } from "../actions";
import { ActivityStatus } from "../../common";

class ForgotPasswordMutation {

  setLoading(state: any, key: string) {
    return {
      ...state,
      forgotPayload: {
        activityStatus: ActivityStatus.Loading,
        error: null,
        payload: {}
      }
    };
  }
  setForgotSucess(state: any, payload: any) {
    return {
      ...state,
      forgotPayload: {
        activityStatus: ActivityStatus.Loaded,
        error: null,
        payload: payload
      }
    };
  }
}

export default new ForgotPasswordMutation;