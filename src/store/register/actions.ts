import { RootActions, IActionTyped } from "../actions";
import { IScheduleParam } from "../../models/schedule";
import _ from "./types";
import { IRegisterModel } from "../../models/register/register.model";

class RegisterAction extends RootActions {

  register(user: IRegisterModel): IActionTyped<IRegisterModel> {
    return { type: _.REGISTER, payload: user }
  }

}

export default new RegisterAction()