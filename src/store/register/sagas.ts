import { IActionTyped } from "../actions";
import { takeLatest, put } from "@redux-saga/core/effects";
import _ from "./types";
import actions from "./actions";
import { LoginService, ILoginService } from "../../services";
import { IRegisterService } from "../../services/register.service";
import { RegisterService } from "../../services/register.service";
import { IRegisterModel } from "../../models/register";

const service: IRegisterService = new RegisterService()

function* register(action: IActionTyped<IRegisterModel>) {
  try {
    yield put(actions.setLoading(_.SET_ACTIVITY_STATUS))//set trang thai loading cua man hinh
    const result = yield service.register(action.payload!)//
    yield put({ type: _.REGISTER_SUCCESS, payload: result })
  } catch (error) {
    yield put(actions.setError(_.REGISTER_FAILED, error))
  }
}

export function* watchRegister() {
  yield takeLatest(_.REGISTER, register)
}
