import { ActivityStatus } from "../../common";

export interface IRegisterState {
  registerPayload: {
    activityStatus: ActivityStatus,
    payload: any,
    error: any | null
  }
}

export class RegisterState implements IRegisterState {
  registerPayload: {
    activityStatus: ActivityStatus,
    payload: any,
    error: any | null
  }
  /**
   *
   */
  constructor() {
    this.registerPayload = {
      activityStatus: ActivityStatus.NoActivity,
      error: null,
      payload: {}
    }
  }
}