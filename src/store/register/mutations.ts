import { RootActions } from "../actions";
import { ActivityStatus } from "../../common";

class RegisterMutation {

  setLoading(state: any, key: string) {
    return {
      ...state,
      registerPayload: {
        activityStatus: ActivityStatus.Loading,
        error: null,
        payload: {}
      }
    };
  }
  setRegisterSucess(state: any, payload: any) {
    return {
      ...state,
      registerPayload: {
        activityStatus: ActivityStatus.Loaded,
        error: null,
        payload: payload
      }
    };
  }
}

export default new RegisterMutation;