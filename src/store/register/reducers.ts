import {  IRegisterState, RegisterState } from "./state";
import { IActionTyped } from "../actions";
import _ from "./types";
import mutation from "./mutations";

const initialState: IRegisterState = new RegisterState()

export default (state: IRegisterState = initialState, action: IActionTyped) => {

  switch (action.type) {
    case _.SET_ACTIVITY_STATUS:
      return mutation.setLoading(state, 'registerPayload')
    case _.REGISTER_SUCCESS:
      return mutation.setRegisterSucess(state, action.payload)
    case _.REGISTER_FAILED:
      return state
    default:
      return state
  }

}