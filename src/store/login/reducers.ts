import { IActionTyped } from "../actions";
import _ from "./types";
import mutation from "./mutations";
import { ILoginState, LoginState } from "../login/state";
import { ILoginParam } from "../../models/login";

// enum ScheduleKeys {
//   listSchedules = "scheduleStore"
// }
const initialState: ILoginState = new LoginState()

export default (state: ILoginState = initialState, action: IActionTyped) => {

  switch (action.type) {
    case _.SET_ACTIVITY_STATUS:
      return mutation.setLoading(state, 'loginPayload')
    case _.LOGIN_SUCCESS:
      return mutation.setLogin(state, action.payload)
    case _.LOGIN_FAILED:
      return state
    default:
      return state
  }

}