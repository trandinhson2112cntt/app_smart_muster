import { RootActions, IActionTyped } from "../actions";
import _ from "./types";
import { ILoginParam } from "../../models/login";

class LoginAction extends RootActions {

  login(param: ILoginParam): IActionTyped<ILoginParam> {
    return { type: _.LOGIN, payload: param }
  }

}

export default new LoginAction()