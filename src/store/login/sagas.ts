import { IActionTyped } from "../actions";
import { takeLatest, put } from "@redux-saga/core/effects";
import _ from "./types";
import actions from "./actions";
import { ILoginService } from "../../services";
import { LoginService } from "../../services";
import { ILoginParam } from "../../models/login";
import {NavigationService} from '../../services/navigation.service';
const service: ILoginService = new LoginService(); 
function* login(action: IActionTyped<ILoginParam>) {
  try {
    yield put(actions.setLoading(_.SET_ACTIVITY_STATUS))
    
    const result = yield service.login(action.payload!)
    yield put({ type: _.LOGIN_SUCCESS, payload: result})
    yield NavigationService.navigate('App');
  } catch (error) {
    yield put(actions.setError(_.LOGIN_FAILED, error))
  }
}

export function* watchLogin() {
  yield takeLatest(_.LOGIN, login)
}