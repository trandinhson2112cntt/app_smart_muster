import { RootActions } from "../actions";
import { ActivityStatus } from "../../common";
import { ILoginParam } from "../../models/login";
import { ILoginState } from "./state";

class LoginMutation {

  setLoading(state: ILoginState, key: string) {
    return {
      ...state,
      loginPayload: {
        activityStatus: ActivityStatus.Loading,
        error: null,
        payload: {}
      }
    };
  }
  setLogin(state: ILoginState, payload: any) {
    return {
      ...state,
      loginPayload: {
        activityStatus: ActivityStatus.Loaded,
        error: null,
        payload: payload
      }
    };
  }
}

export default new LoginMutation;