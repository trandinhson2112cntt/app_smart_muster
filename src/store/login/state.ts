import { ActivityStatus } from "../../common";
import { ILoginParam, LoginParam } from "../../models/login";

export interface ILoginState {
  loginPayload: {
    activityStatus: ActivityStatus,
    payload: ILoginParam,
    error: any | null
  }
}

export class LoginState implements ILoginState {
  loginPayload: {
    activityStatus: ActivityStatus,
    payload: ILoginParam | any,
    error: any | null
  }
  /**
   *
   */
  constructor() {
    this.loginPayload = {
      activityStatus: ActivityStatus.NoActivity,
      error: null,
      payload: {}
    }
  }
}