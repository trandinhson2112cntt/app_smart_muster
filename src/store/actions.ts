import { ActivityStatus } from "../common";
import { IExceptionViewModel } from "../models/generic";

export interface IActionTyped<T = {}> {
  payload?: T | null//undefined //null
  type: string
  meta?: any
  error?: any | null
}

export interface IRootActions {
}
export class RootActions implements IRootActions {

  setLoading(type: string) {
    return { type: type, payload: ActivityStatus.Loading }
  }

  setError(type: string, error: IExceptionViewModel) {
    return { type, payload: ActivityStatus.LoadedFailed, error }
  }

}
