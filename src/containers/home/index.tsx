import React, { Component } from 'react';
import { Props, State } from './state-props';
import { View, Text, Image, TouchableOpacity, FlatList } from 'react-native';
import styles from "./styles";
import { aStyles } from '../../styles';
import { Header } from '../../components';
import { Icon } from 'native-base';

const data = [
  { Id: '1', Lesson: 'Tiết 1', TimeStart: '16:00', TimeEnd: '19:25', NameSubject: 'Mạng máy tính', Room: 'B11.01', Class: 'Lớp 15DTH13' },
  { Id: '2', Lesson: 'Tiết 4', TimeStart: '14:45', TimeEnd: '17:00', NameSubject: 'Lập trình C', Room: 'B12.03', Class: 'Lớp 15DTH13' },
  { Id: '3', Lesson: 'Tiết 7', TimeStart: '16:30', TimeEnd: '19:00', NameSubject: 'Cơ sở dữ liệu', Room: 'B9.01', Class: 'Lớp 15DTH13' },
  { Id: '4', Lesson: 'Tiết 4', TimeStart: '09:00', TimeEnd: '17:25', NameSubject: 'Mạng máy tính', Room: 'B8.01', Class: 'Lớp 15DTH13' },
  { Id: '5', Lesson: 'Tiết 4', TimeStart: '06:00', TimeEnd: '11:25', NameSubject: 'Mạng máy tính', Room: 'B8.01', Class: 'Lớp 15DTH13' },
  { Id: '6', Lesson: 'Tiết 4', TimeStart: '07:00', TimeEnd: '11:25', NameSubject: 'Mạng máy tính', Room: 'B8.01', Class: 'Lớp 15DTH13' },
  { Id: '7', Lesson: 'Tiết 4', TimeStart: '09:00', TimeEnd: '11:25', NameSubject: 'Mạng máy tính', Room: 'B8.01', Class: 'Lớp 15DTH13' },
  { Id: '8', Lesson: 'Tiết 4', TimeStart: '09:00', TimeEnd: '11:25', NameSubject: 'Mạng máy tính', Room: 'B8.01', Class: 'Lớp 15DTH13' },
  { Id: '9', Lesson: 'Tiết 4', TimeStart: '09:00', TimeEnd: '11:25', NameSubject: 'Mạng máy tính', Room: 'B8.01', Class: 'Lớp 15DTH13' },
  { Id: '10', Lesson: 'Tiết 4', TimeStart: '09:00', TimeEnd: '11:25', NameSubject: 'Mạng máy tính', Room: 'B8.01', Class: 'Lớp 15DTH13' },
]
class HomeContainer extends React.Component<Props, State>{
  static navigationOptions = {
    tabBarIcon: (tintColor: any) => {  return (
      <Icon name='home' type='FontAwesome' style={{ textAlign:'center',color: tintColor.tintColor }}/>
    )},
    header: null
  }
  /**
   *
   */
  constructor(props: Readonly<Props>) {
    super(props);
  }

   myFunction() {
    var d = new Date();
    var n = d.getTime();
    return d.getHours() + ":" + d.getMinutes();
    // document.getElementById("demo").innerHTML = time;
  }

  render() {
    return (
      <View style={styles.containerStyle}>
        <Header title={''} isCalendar={true} isBack={false} isNULL={false} isNotification={true} />
        <View style={styles.flatList}>
          <FlatList 
            data={data}
            renderItem={this._renderItemList.bind(this)}
            keyExtractor={(item,index) => item.Id}
            ListHeaderComponent = {<Text>Thứ 2 - Ngày 15/01/2019</Text>}
          />
        </View>
      </View>
    )
  }

  _renderItemList({item}) {
    return (
      <TouchableOpacity style={styles.flatListItem}>
            <View style={styles.timeItem}>
              <View style={styles.titelTimeItem}>
                <Text>{item.Lesson}</Text>
              </View>
              <View style={styles.timeToTimeItem}>
                <View style={styles.timeFrom}>
                  <Icon name='dot-single' type='Entypo' style={{ flex: 1, color: '#b0b0ff', fontSize: 54, textAlign: 'center' }} />
                  <Text style={{ flex: 2, textAlign: 'left', }}>{item.TimeStart}</Text>
                </View>

                <View style={styles.space}>
                  <View style={styles.leftTime}>
                    <View style={{ flex: 1, borderWidth: 0.5, borderColor: '#d4d4d4' }}>
                    </View>
                  </View>
                  <View style={styles.rightTime}>

                  </View>
                </View>
                
                <View style={styles.timeFrom}>
                  <Icon name='dot-single' type='Entypo' style={{ flex: 1, color: '#92DF4E', fontSize: 54, textAlign: 'center' }} />
                  <Text style={{ flex: 2, textAlign: 'left', }}>{item.TimeEnd}</Text>
                </View>
              </View>
            </View>
            <View style={[styles.infoItem,  this.myFunction() > item.TimeStart && this.myFunction() < item.TimeEnd ? {borderRightColor: '#00FF00'} : {borderRightColor: '#d4d4d4'}]}>
              <View style={styles.headerInfo}>
                <Text style={[aStyles.fontText,styles.txtHeader]}>{item.NameSubject}</Text>
              </View>
              <View style={styles.bodyInfo}>
                <Text style={[aStyles.fontText,styles.txtBody]}>{item.Room}</Text>
              </View>
              <View style={styles.footerInfo}>
                <Text style={[aStyles.fontText,styles.txtFooter]}>{item.Class}</Text>
              </View>
            </View>
          </TouchableOpacity>
    )
  }
}

export default HomeContainer;