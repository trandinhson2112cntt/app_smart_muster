import { IRegisterModel } from "../../models/register";

export interface Props { 
    handleDispatchRegister: (user: IRegisterModel) => void;
    register?: any
}
export interface State { 
    isTeacher: boolean,
    isStudent: boolean,
    code: string,
    password: string,
    name:string,
    address:string,
    phone:string,
    email:string,
    typeUser:string,
    err: string
}