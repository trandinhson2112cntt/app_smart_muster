import { StyleSheet } from "react-native";
import { colors } from "../../styles";

export default  StyleSheet.create({
    header: {
      flex:1,
      flexDirection: 'row',
    },
  
    headerLeft:{
      flex:1,
      justifyContent: 'center',
      marginLeft: 10,
     },
    headRight:{
      flex:1,
      padding: 12,
      marginLeft: 40,
    },
    body:{
      backgroundColor: 'white',
      flex:2.6,
      marginLeft: 20,
      marginRight: 20,
      borderRadius: 15,
      paddingRight: 30,
      marginTop: -1,
    },
    footer :{
      flex:1,
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
    },
    footerLeft:{
      flex:1,
      alignItems: 'center',
      justifyContent: 'center',
      
    },
    footerRight:{
      flex:1,
      alignItems: 'center',
      justifyContent: 'center',
      
    },
    title:{
      color: colors.Black,
      fontSize: 27,
      fontFamily: 'OpenSans-Regular',
      
    },
    subTitle:{
      color: colors.Black,
      fontSize:20,
      fontFamily: 'OpenSans-Regular',
    },
    card:{
      backgroundColor:'gray',
    },
    userTitle:{
      fontSize: 18,
      fontFamily: 'OpenSans-Regular',
      color:'gray',
      borderBottomWidth: 0.5,
      borderBottomColor: '#ddd',
      paddingRight: 50,
      flex:8
    },
    IconUser:{
      color:'#384850',
      flex:1,
      alignItems: 'center',
      justifyContent: 'center',
      textAlign:'center',
    },
    inputGroup:{
      flex:1,
      flexDirection: 'row',
      
    },
    radioGroup:{
      flex:1,
      alignItems: 'center',

    }
  
    
  })
