import React, { Component, Dispatch } from 'react';
import { Props, State } from './state-props';
import { View, Text, ImageBackground, Image, Alert } from 'react-native';
import _ from "./styles";
import { NavigationService } from '../../services';
import { InputGroup, Icon, Input, Radio,Button } from 'native-base';
import { Values, ActivityStatus } from '../../common';
import { IRegisterModel, RegisterModel } from '../../models/register';
import { connect } from 'react-redux';
import { IRootReducer } from '../../store/reducers';
import { IActionTyped } from '../../store/actions';
import actions from '../../store/register/actions';

class RegisterContainer extends React.Component<Props, State>{

  static navigationOptions={
    header: null
  }
  constructor(props: Readonly<Props>) {
    super(props);
    this.state={
      isTeacher: false,
      isStudent: true,
      code: "",
      password:"",
      name:"",
      address:"",
      phone:"",
      email:"",
      typeUser:"",
      err:""
    }
    this.handleRegister = this.handleRegister.bind(this)
  }
  componentWillReceiveProps(nextProps: Props) {
    if (nextProps.register!.activityStatus === ActivityStatus.Loaded) {
        Alert.alert("Thông tin sinh viên vừa đăng ký",JSON.stringify(nextProps.register!.payload.data))
    }
  }
  async handleRegister() {
    const user: IRegisterModel = new RegisterModel({// 1 object
      code: this.state.code,
      password: this.state.password,
      name: this.state.name,
      address: this.state.address,
      phone: this.state.phone,
      email: this.state.email,
      typeUser: this.state.typeUser
    });

    await RegisterModel.validateData(user)
      .then((errors) => {
        this.setState({err: errors });
        Alert.alert("Lỗi",this.state.err);
      })
    if(this.state.err === "")
      this.props.handleDispatchRegister(user)

   
  }

  render() {
    return (
      <View style={{ height: Values.FullHeight,width: Values.FullWidth}}>
      <ImageBackground source={require('../../assets/images/background.jpg')} style={{width:'100%',height:'100%'}}>
                  <View style={_.header}>
                    <View style={_.headerLeft}>
                      <Text style={_.title}>ĐĂNG KÝ</Text>
                      <Text style={_.subTitle}>Study</Text>

                    </View>
                    <View style={_.headRight}>
                      <Image source={require('../../assets/images/logo_hutech.png')} style={{width:100,height:110,marginLeft:20}}></Image>
                                          
                    </View>
                  </View>
                  
                  <View style={_.body}>
                    
                        <InputGroup style={_.inputGroup}>
                          <Icon name='user' style={_.IconUser} type='FontAwesome'/>
                          <Input style={_.userTitle} placeholder='Mã số' onChangeText={(text) => this.setState({code:text})}/>
                        </InputGroup>
                      
                      <InputGroup style={_.inputGroup}>
                          <Icon name='lock' style={_.IconUser} type='MaterialIcons'/>
                          <Input style={_.userTitle} placeholder='Mật khẩu' secureTextEntry={true} onChangeText={(text) => this.setState({password:text})}/>
                        </InputGroup>
                      
                      <InputGroup style={_.inputGroup}>
                          <Icon name='idcard' style={_.IconUser} type='AntDesign'/>
                          <Input style={_.userTitle} placeholder='Họ và tên' onChangeText={(text) => this.setState({name:text})}/>
                        </InputGroup>
                      
                      <InputGroup style={_.inputGroup}>
                          <Icon name='location-pin' style={_.IconUser} type='Entypo'/>
                          <Input style={_.userTitle}  placeholder='Địa chỉ' onChangeText={(text) => this.setState({address:text})}/>
                        </InputGroup>
                      
                      <InputGroup style={_.inputGroup}>
                          <Icon name='phone' style={_.IconUser} type='FontAwesome'/>
                          <Input style={_.userTitle} placeholder='Số điện thoại'  onChangeText={(text) => this.setState({phone:text})}/>
                        </InputGroup>
                      
                      <InputGroup style={_.inputGroup}>
                          <Icon name='email' style={_.IconUser} type='Entypo'/>
                          <Input style={_.userTitle} placeholder='Email' onChangeText={(text) => this.setState({email:text})}/>
                        </InputGroup>
                      
                      <InputGroup style={_.inputGroup}>
                        <View style={_.radioGroup}>
                          <Radio  selected={!this.state.isTeacher} onPress={() => this.setState({ isTeacher:false,isStudent:true,typeUser:"Teacher" })}/>
                          <Text style={{color:'gray',fontSize:18,marginLeft:10}}>Giáo viên</Text>
                        </View>
                        <View style={_.radioGroup}>
                          <Radio selected={!this.state.isStudent} onPress={() => this.setState({ isTeacher:true,isStudent:false, typeUser:"Student" })}/>
                          <Text style={{color:'gray',fontSize:18,marginLeft:10}}>Học sinh</Text>
                        </View>
                      </InputGroup>
                      
                     
                  </View>
                  
                  <View style={_.footer}>
                    <View style={_.footerLeft}>
                      <View>
                        <Button rounded style={{width:100, justifyContent:'center',backgroundColor:'#7add81'}}
                          onPress = {()=>{NavigationService.back()}}
                        >
                          <Icon 
                            name = 'arrow-left'
                            color = 'white'
                            type = 'EvilIcons'
                            style={{fontSize:50}}
                          />
                        </Button>
                      </View>
                    </View>
                    <View style={_.footerRight}>
                      <View>
                        <Button 
                          rounded 
                          style={{width:100, justifyContent:'center', backgroundColor:'#7add81'}}
                          onPress = {this.handleRegister}// chuyen vao 1 funtion
                          >
                          <Icon 
                            name = 'check'
                            color = 'white' 
                            type = 'EvilIcons'
                            style={{fontSize:50}}
                          />
                        </Button>
                      </View>
                    </View>
                  </View>
            </ImageBackground>
            </View>
    )
  } 
}
const mapStateToProps= (rootReducer: IRootReducer) =>({
  register: rootReducer.registerReducer.registerPayload   
})
const mapDispatchToProps = (dispatch: Dispatch<IActionTyped>)=>({
  handleDispatchRegister: (user: IRegisterModel) => dispatch(actions.register(user))
})
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RegisterContainer);
