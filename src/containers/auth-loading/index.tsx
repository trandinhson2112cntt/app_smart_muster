import React, { Component } from 'react';
import { Props, State } from './state-props';
import { View, Text, AsyncStorage } from 'react-native';
import styles from "./styles";
import { NavigationService } from '../../services';
import { Screens } from '../../common';

class AuthLoadingContainer extends React.Component<Props, State>{

  /**
   *
   */
  constructor(props: Readonly<Props>) {
    super(props);
  }

  componentDidMount() {
    
    setTimeout(() => {
      AsyncStorage.getItem("access_token")
      .then(res => {
        if(res !== null)
          NavigationService.navigate('App');
        else
          NavigationService.navigate('Auth');
      })
    }, 2400);
  }
  render() {
    return (
      <View
        style={styles.containerStyle}
      >
        <Text>HELLO</Text>
      </View>
    )
  }
}

export default AuthLoadingContainer;