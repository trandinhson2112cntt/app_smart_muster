import React, { Component } from 'react';
import { Props, State } from './state-props';
import { View, Text } from 'react-native';
import styles from "./styles";
import { Icon } from 'native-base';

class StudyContainer extends React.Component<Props, State>{
  static navigationOptions = {
    tabBarIcon: (tintColor: any) => {  return (
      <Icon name='school' type='MaterialCommunityIcons' style={{ textAlign:'center',color: tintColor.tintColor }}/>
    )}
  }
  /**
   *
   */
  constructor(props: Readonly<Props>) {
    super(props);
  }
  render() {
    return (
      <View
        style={styles.containerStyle}
      >
        <Text>hello boilerplate</Text>
      </View>
    )
  }
}

export default StudyContainer;