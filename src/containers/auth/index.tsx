import React, { Component, Dispatch } from 'react';
import { Props, State } from './state-props';
import { View, Text, ImageBackground, Image, TouchableOpacity, Alert } from 'react-native';
import styles from "./styles";
import { NavigationService, LoginService } from '../../services';
import { Screens, Values } from '../../common';
import { aStyles, colors } from '../../styles';
import { InputGroup, Icon, Input, Button } from 'native-base';
import { LoginParam, ILoginParam } from '../../models/login';
import { IRootReducer } from '../../store/reducers';
import { IActionTyped } from '../../store/actions';
import { connect } from 'react-redux';
import actions from '../../store/login/actions';
import { Validator, validate } from 'class-validator';
class AuthContainer extends React.Component<Props, State>{
  static navigationOptions={
    header: null
  }
  constructor(props: Readonly<Props>) {
    super(props);
    this.state = {
      code: "",
      password: "",
      err: ""
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentWillReceiveProps(nextProps: Props) {
    if(nextProps.loginPayload!.payload.message === "OK"){
      console.log('====================================');
      console.log(nextProps.loginPayload!.payload.data.access_token);
      console.log('====================================');
      // Alert.alert( "This is token",nextProps.loginPayload!.payload.data.access_token +"Type: "+ nextProps.loginPayload!.payload.data.typeUser);
    }
    else
      this.setState({err: "Tài khoản hoặc mật khẩu không chính xác!"})
  }
  
  async handleSubmit() {
    const userLogin = new LoginParam({ code: this.state.code, password: this.state.password})
    await LoginParam.validateData(userLogin)
      .then((errors) => {this.setState({err: errors })})
    if(this.state.err === "")
      this.props.handleLogin(userLogin)
  }
  goToRegisterScreen() {
    NavigationService.navigate(Screens.Register)
  }
  goToForgotPasswordScreen() {
    NavigationService.navigate(Screens.ForgotPassword)
  }

  render() {
    return (
      <View style={styles.containerStyle}>
        <ImageBackground source={require('../../assets/images/background.jpg')} style={styles.imgBackground}>
          <View style={styles.header}>
            <Image source={require('../../assets/images/logo_hutech.png')} style={styles.logo}/>
            <Text style={[styles.titleLogo,aStyles.fontText]}>Study</Text>
          </View>
          <View style={styles.body}>
            <View style={styles.form}>
              <View style={styles.row}>
                <InputGroup borderType='underline' >
                  <Icon name='user' style={{color:colors.Gray}} type='FontAwesome'/>
                  <Input placeholder='Mã số' onChangeText={(text) => this.setState({code:text})}/>
                </InputGroup>
              </View>
              <View style={styles.row}>
                <InputGroup borderType='underline' >
                  <Icon name='lock' style={{color:colors.Gray}} type='FontAwesome'/>
                  <Input placeholder='Mật khẩu' secureTextEntry={true} onChangeText={(text) => this.setState({password:text})}/>
                </InputGroup>
              </View>
              <View style={[{flex:0.5,flexDirection: 'row'}]}>
                <TouchableOpacity style={styles.footerBody} onPress={() => {this.goToRegisterScreen()}}>
                  <Text style={[styles.footerBodyLeft,aStyles.fontText]}>Đăng ký</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.footerBody} onPress={() => {this.goToForgotPasswordScreen()}}>
                  <Text style={[styles.footerBodyRight,aStyles.fontText]}>Quên mật khẩu</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <View>
            <Text style={styles.notification}>{this.state.err}</Text>
          </View>
          <View style={styles.footer}>
            <View>
              <Button rounded style={styles.buttonSubmit} onPress={this.handleSubmit}>
                <Icon name='arrow-right' type='FontAwesome' style={{ textAlign:'center' }}/>
              </Button>
            </View>
          </View>
        </ImageBackground>
      </View>
    )
  }
}

const mapStateToProps = (rootReducer: IRootReducer) => ({
  loginPayload: rootReducer.loginReducer.loginPayload
})
const mapDispatchToProps = (dispatch: Dispatch<IActionTyped>) => ({
  handleLogin: (info: ILoginParam) => dispatch(actions.login(info))
})
export default connect(mapStateToProps,mapDispatchToProps)(AuthContainer);
