import { ILoginParam } from "../../models/login";

export interface Props { 
    handleLogin: (info: ILoginParam) => void,
    loginPayload?: any
}
export interface State {
    code: string,
    password: string,
    err: string
}