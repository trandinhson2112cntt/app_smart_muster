import { StyleSheet } from "react-native";
import { Values } from "../../common";
import { colors } from "../../styles";

export default StyleSheet.create({
  containerStyle: {},
  imgBackground: {
    width: Values.FullWidth,
    height: Values.FullHeight
  },
  header: {
    flex:3,
    alignItems:'center',
    justifyContent: 'center',
  },
  body:{
    flex:3,
  },
  footer:{
    flex:2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo:{
    width:120,
    height:130
  },
  titleLogo:{
    fontSize:26,
    color: colors.Black
  },
  form:{
    backgroundColor: colors.White,
    borderRadius: 10,
    flex:1,
    marginTop: -1,
    marginBottom: 10,
    marginHorizontal: 10,
  },
  row:{
    flex:1,
    justifyContent: 'center',
    marginHorizontal: 20,
  },
  footerBody:{
    flex:1,
  },
  footerBodyRight:{
    textAlign:'right',
    marginRight: 15,
    fontSize: 16,
    fontStyle: 'italic',
    textDecorationLine:'underline'
  },
  footerBodyLeft:{
    marginLeft: 15,
    textAlign:'left',
    fontSize: 16,
    fontStyle: 'italic',
    textDecorationLine:'underline'
  },
  notification:{
    fontSize: 12,
    color: colors.Red,
    textAlign:'center',
    fontStyle:'italic'
  },
  buttonSubmit: {
    backgroundColor: '#7add81',
    width:80, 
    justifyContent:'center'
  }
})