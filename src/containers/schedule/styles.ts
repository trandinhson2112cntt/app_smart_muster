import { StyleSheet } from "react-native";
import { colors } from "../../styles";

export default StyleSheet.create({
  containerStyle: {
    flex:1,
    backgroundColor: 'white',
  },
  pickerContainer:{
    height: 80,
    flexDirection: 'row',
    
    justifyContent: 'center',
    alignItems: 'center',
  },
  picker: {
    flex:1,
    marginVertical: 10,
    borderWidth: 1,
    borderRadius: 20,
    marginHorizontal: 10,
    borderColor: '#d4d4d4',
  },
  listDay: {
    height: 80,
    paddingVertical: 10,
    borderWidth: 1,
    borderRadius: 15,
    marginHorizontal: 10,
    borderColor: '#d4d4d4',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  listDayItem: {
    flex:1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  listDayText: {
    flex:1
  },
  listDayIcon: {
    flex:1,
    color: '#FB7A7A'
  },
  flatList: {
    flex: 1,
    marginHorizontal: 10,
    marginTop: 10,
  },
  flatListItem: {
    flexDirection: 'row',
    height: 100,
    marginBottom: 10,
  },
  timeItem:{
    flex:1,
    borderWidth: 1,
    borderRadius: 15,
    borderColor: '#d4d4d4',
    // shadowColor: '#d4d4d4',
    //     shadowOpacity: 0.3,
    //     // shadowRadius: 2,
    //     shadowOffset: {
    //         height: 5,
    //         width: 5
    //     },
    //     elevation:1
  },
  infoItem:{
    flex:3,
    borderWidth: 1,
    borderRadius: 15,
    marginLeft: 5,
    borderColor: '#d4d4d4',
    // shadowColor: '#d4d4d4',
    //     shadowOpacity: 0.3,
    //     // shadowRadius: 2,
    //     shadowOffset: {
    //         height: 5,
    //         width: 5
    //     },
    //     elevation:1
  },
  titelTimeItem:{
    flex:1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  timeToTimeItem: {
    flex:3,
    justifyContent: 'center',
    alignItems: 'center',
  },
  timeFrom:{
    flex:1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  leftTime: {
    flex:1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  rightTime: {
    flex:2
  },
  space:{
    flex:2,
    flexDirection: 'row',
  },
  headerInfo: {
    flex:1,
    justifyContent: 'center',
    paddingLeft: 10,
  },
  bodyInfo: {
    flex:1,
    justifyContent: 'center',
    paddingLeft: 10,
  },
  footerInfo: {
    flex:1,
    justifyContent: 'center',
    paddingLeft: 10,
  },
  txtHeader:{
    color: colors.Black,
    fontSize: 18
  },
  txtBody:{
    fontSize: 14,
    color: '#F89329'
  },
  txtFooter:{
    color: colors.Black,
    fontSize: 14
  }
})