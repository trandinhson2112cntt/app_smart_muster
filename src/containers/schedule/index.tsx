import React, { Component, Dispatch } from 'react';
import { Props, State } from './state-props';
import { View, Text, Picker, TouchableOpacity, FlatList } from 'react-native';
import styles from "./styles";
import { IRootReducer } from '../../store/reducers';
import { IActionTyped } from '../../store/actions';
import { connect } from 'react-redux';
import { IScheduleParam } from '../../models/schedule';
import { ScheduleActions } from '../../store/schedule';
import { Icon } from 'native-base';
import { Header } from '../../components';
import { aStyles } from '../../styles';

const data = [
  { Id: '1', Lesson: 'Tiết 1', TimeStart: '9:00', TimeEnd: '11:25', NameSubject: 'Mạng máy tính', Room: 'B11.01', Class: 'Lớp 15DTH13' },
  { Id: '2', Lesson: 'Tiết 4', TimeStart: '6:45', TimeEnd: '9:00', NameSubject: 'Lập trình C', Room: 'B12.03', Class: 'Lớp 15DTH13' },
  { Id: '3', Lesson: 'Tiết 7', TimeStart: '12:30', TimeEnd: '3:00', NameSubject: 'Cơ sở dữ liệu', Room: 'B9.01', Class: 'Lớp 15DTH13' },
  { Id: '4', Lesson: 'Tiết 4', TimeStart: '9:00', TimeEnd: '11:25', NameSubject: 'Mạng máy tính', Room: 'B8.01', Class: 'Lớp 15DTH13' },
]
class ScheduleContainer extends React.Component<Props, State>{
  static navigationOptions = {
    tabBarIcon: (tintColor: any) => {
      return (
        <Icon name='calendar' type='AntDesign' style={{ textAlign: 'center', color: tintColor.tintColor }} />
      )
    }
  }
  /**
   *
   */
  constructor(props: Readonly<Props>) {
    super(props);

  }
  render() {

    return (
      <View style={styles.containerStyle}>
        <Header title={'Học kỳ \n2019-2020'} isNull={false} isCalendar={false} isBack={false} isNotification={false} />
        <View style={styles.pickerContainer}>
          <View style={styles.picker}>
            <Picker
            // selectedValue={this.state.language}
            // style={{height:20}}
            // onValueChange={(itemValue, itemIndex) =>
            //   this.setState({ language: itemValue })
            // }
            >
              <Picker.Item label="Tháng 1" value="1" />
              <Picker.Item label="Tháng 2" value="2" />
            </Picker>
          </View>
          <View style={styles.picker}>
            <Picker
            // selectedValue={this.state.language}
            //style={styles.picker}
            // onValueChange={(itemValue, itemIndex) =>
            //   this.setState({ language: itemValue })
            // }
            >
              <Picker.Item label="Tuần 1" value="1" />
              <Picker.Item label="Tuần 2" value="2" />
            </Picker>
          </View>
        </View>
        <View style={styles.listDay}>
          <TouchableOpacity style={styles.listDayItem}>
            <Text style={styles.listDayText}>T2</Text>
            <Icon name='dot-single' type='Entypo' style={styles.listDayIcon} />
          </TouchableOpacity>
          <TouchableOpacity style={styles.listDayItem}>
            <Text style={styles.listDayText}>T3</Text>
            <Icon name='dot-single' type='Entypo' style={styles.listDayIcon} />
          </TouchableOpacity>
          <TouchableOpacity style={styles.listDayItem}>
            <Text style={styles.listDayText}>T4</Text>
            <View style={styles.listDayIcon}></View>
            {/* <Icon name='dot-single' type='Entypo'/> */}
          </TouchableOpacity>
          <TouchableOpacity style={styles.listDayItem}>
            <Text style={styles.listDayText}>T5</Text>
            <Icon name='dot-single' type='Entypo' style={styles.listDayIcon} />
          </TouchableOpacity>
          <TouchableOpacity style={styles.listDayItem}>
            <Text style={styles.listDayText}>T6</Text>
            <View style={styles.listDayIcon}></View>

            {/* <Icon name='dot-single' type='Entypo'/> */}
          </TouchableOpacity>
          <TouchableOpacity style={styles.listDayItem}>
            <Text style={styles.listDayText}>T7</Text>
            <View style={styles.listDayIcon}></View>

            {/* <Icon name='dot-single' type='Entypo'/> */}
          </TouchableOpacity>
          <TouchableOpacity style={styles.listDayItem}>
            <Text style={styles.listDayText}>CN</Text>
            <Icon name='dot-single' type='Entypo' style={styles.listDayIcon} />
          </TouchableOpacity>
        </View>
        <View style={styles.flatList}>
          <FlatList
            data={data}
            renderItem={this._renderItemList.bind(this)}
            keyExtractor={(item, index) => item.Id}
            ListHeaderComponent={<Text>Thứ 2 - Ngày 15/01/2019</Text>}
          />
        </View>
      </View>
    )
  }

  _renderItemList({ item }) {
    return (
      <TouchableOpacity style={styles.flatListItem}>
        <View style={styles.timeItem}>
          <View style={styles.titelTimeItem}>
            <Text>{item.Lesson}</Text>
          </View>
          <View style={styles.timeToTimeItem}>
            <View style={styles.timeFrom}>
              <Icon name='dot-single' type='Entypo' style={{ flex: 1, color: '#b0b0ff', fontSize: 54, textAlign: 'center' }} />
              <Text style={{ flex: 2, textAlign: 'left', }}>{item.TimeStart}</Text>
            </View>
            <View style={styles.space}>
              <View style={styles.leftTime}>
                <View style={{ flex: 1, borderWidth: 0.5, borderColor: '#d4d4d4' }}>
                
                </View>
              </View>
              <View style={styles.rightTime}>

              </View>
            </View>
            <View style={styles.timeFrom}>
              <Icon name='dot-single' type='Entypo' style={{ flex: 1, color: '#92DF4E', fontSize: 54, textAlign: 'center' }} />
              <Text style={{ flex: 2, textAlign: 'left', }}>{item.TimeEnd}</Text>
            </View>
          </View>
        </View>
        <View style={styles.infoItem}>
          <View style={styles.headerInfo}>
            <Text style={[aStyles.fontText, styles.txtHeader]}>{item.NameSubject}</Text>
          </View>
          <View style={styles.bodyInfo}>
            <Text style={[aStyles.fontText, styles.txtBody]}>{item.Room}</Text>
          </View>
          <View style={styles.footerInfo}>
            <Text style={[aStyles.fontText, styles.txtFooter]}>{item.Class}</Text>
          </View>
        </View>
      </TouchableOpacity>
    )
  }
}

const mapStateToProps = (rootReducers: IRootReducer) => ({

})

const mapDispatchToProps = (dispatch: Dispatch<IActionTyped>) => ({
  getSchedules: (param: IScheduleParam) => dispatch(ScheduleActions.getSchedules(param))
})


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ScheduleContainer);