import React, { Component } from 'react';
import { Props, State } from './state-props';
import { View, Text } from 'react-native';
import styles from "./styles";
import { aStyles } from '../../styles';
import { Header } from '../../components';

class NotificationContainer extends React.Component<Props, State>{
  static navigationOptions={
    header: null
  }
  /**
   *
   */
  constructor(props: Readonly<Props>) {
    super(props);
  }
  render() {
    return (
      <View style={styles.containerStyle}>
        <Header title={'Thông báo'} isBack ={true} isNotification={true}/>
        <Text style={{paddingLeft:10}}>Gần đây</Text>
        <Text>Trước đó</Text>
        
      </View>
    )
  }
}

export default NotificationContainer;