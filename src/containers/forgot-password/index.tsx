import React, { Component, Dispatch } from 'react';
import { Props, State } from './state-props';
import { View, Text, ImageBackground, Image, Alert } from 'react-native';
import styles from "./styles";
import { NavigationService } from '../../services';
import { Item, Input, Icon, Button } from 'native-base';
import { Validator, validate } from 'class-validator';
import { LoginParam } from '../../models/login';
import { IRootReducer } from '../../store/reducers';
import { IActionTyped } from '../../store/actions';
import { connect } from 'react-redux';
import actions from '../../store/forgotPassword/actions';
import { ActivityStatus } from '../../common';



class ForgotPasswordContainer extends React.Component<Props, State>{
    static navigationOptions = {
        header: null
    }

    constructor(props: Readonly<Props>) {
        super(props);
        this.state = {
            email: '',
            err: '',
            validated: false,
        }

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentWillReceiveProps(nextProps: Props) {
        console.log(nextProps.forgotPassword)
        if (nextProps.forgotPassword!.activityStatus === ActivityStatus.Loaded) {
            Alert.alert('OK')
        }
    }
    validate = (text: string) => {

        this.setState({ email: text })
    }

    handleSubmit = async () => {
        const validator = new Validator()
        const valid = validator.isEmail(this.state.email, { allow_utf8_local_part: true });
        if (valid) {
            this.setState({ err: ''})
            this.props.handleForgotPassword(this.state.email)
            
        }
        else{
            this.setState({ err: 'Nhập sai định dạng email' })
        }
    }
    render() {
        return (
            <View>
                <ImageBackground source={require('../../assets/images/background.jpg')} style={styles.backgroundImage}>
                    <View style={styles.content}>
                        <View style={styles.contentHeader}>
                            <View style={styles.contentHeaderText}>
                                <Text style={styles.contentHeaderText1}>QUÊN MẬT KHẨU</Text>
                                <Text style={styles.contentHeaderText2}>Study</Text>
                            </View>
                            <Image source={require('../../assets/images/logo_hutech.png')} style={{ width: 100, height: 100 }} />
                        </View>
                        <View style={styles.contentBody}>
                            <View style={styles.contentBodyText}>
                                <Text style={styles.contentBodyTextDetail}>Vui lòng nhập đúng địa chỉ email</Text>
                                <Text style={styles.contentBodyTextDetail}>Sau đó kiểm tra hộp thư mail</Text>
                                <Text style={styles.contentBodyTextDetail}>để nhận lại mật khẩu!</Text>
                            </View>
                            <Item style={styles.contentBodyItems}>
                                <Icon style={styles.contentBodyItemsIcon} type='EvilIcons' name='envelope' />
                                <Input style={[styles.contentBodyItemsInput,]}
                                    onChangeText={(text) => this.validate(text)}
                                    placeholder='Email' />
                                
                            </Item>
                            <Text style={styles.notification}>{this.state.err}</Text>
                            <View>
                            
                                <Button style={styles.contentBodyButtonCheck}
                                    onPress={this.handleSubmit}
                                    rounded success>
                                    <Icon type='EvilIcons' name="check" style={{ fontSize: 40 }} />
                                </Button>
                            </View>
                        </View>
                        <View style={styles.contentFooter}>
                            <Button style={styles.contentFooterButtonBack} rounded success
                                onPress={() => { NavigationService.back() }}
                            >
                                <Icon type='EvilIcons' name="arrow-left" style={{ fontSize: 40 }} />
                            </Button>
                        </View>
                    </View>
                </ImageBackground>
            </View>
        )
    }
}


const mapStateToProps = (rootReducer: IRootReducer) => ({
    forgotPassword: rootReducer.forgotPasswordReducer.forgotPayload
})

const mapDispatchToProps = (dispatch: Dispatch<IActionTyped>) => ({
    handleForgotPassword: (email: string) => dispatch(actions.forgotPassword(email)) 
})
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ForgotPasswordContainer);
