import { StyleSheet, Dimensions } from "react-native";
import { colors } from "../../styles";

export default StyleSheet.create({
  backgroundImage: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    flexDirection: 'column',
    },
    content: {
        flex: 1,
        flexDirection: 'column',
        //justifyContent: 'center',
        alignItems: 'stretch',
    },
    contentHeader: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        //backgroundColor: 'red',
    },
    contentHeaderText: {
        flexDirection: 'column',
        justifyContent: 'space-around',
        alignItems: 'stretch',
    },
    contentHeaderText1: {
        fontSize: 28,
    },
    contentHeaderText2: {
        fontSize: 18,
    },
    contentBody: {
        flex: 2,
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingTop: 20,
    },
    contentBodyText: {
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    contentBodyTextDetail: {
        color: '#A2A2A2',
        fontSize: 18,
    },
    contentBodyItems: {
        width: 350,
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
        borderWidth: 1,
    },
    contentBodyItemsInput: {
        width: 330,
    },
    contentBodyItemsIcon: {
        color: '#666666',
        fontSize: 40,
        marginRight: -5,
        marginLeft: 10,
    },
    contentBodyButtonCheck: {
        width: 80,
        height: 40,
        marginTop: 30,
        justifyContent: 'center',
    },
    contentFooterButtonBack: {
        width: 80,
        height: 40,
        justifyContent: 'center',
    },
    contentFooter: {
        flex: 2,
        flexDirection: 'column',
        justifyContent: 'space-around',
        alignItems: 'flex-start',
        marginLeft: 20,
    },
    error: {
        borderWidth: 3,
        borderColor: 'red'
    },
    notification:{
        fontSize: 12,
        color: colors.Red,
        textAlign:'center',
        fontStyle:'italic'
    },
})