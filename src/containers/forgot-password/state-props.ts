export interface Props {
    handleForgotPassword: (email: string) => void;
    forgotPassword?: any
}
export interface State {
    email: string,
    err: string,
    validated: boolean,
}