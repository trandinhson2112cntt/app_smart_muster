import { BaseService } from "./base.service";
import { ILoginParam } from "../models/login";
import { NavigationService } from "./navigation.service";
import { Alert } from "react-native";
import { LoginDataResponse } from "../models/login";
import { AsyncStorage} from 'react-native';
export interface ILoginService {
    saveToken: (token: string) => any;
    login: (info: ILoginParam) =>  Promise<LoginDataResponse>;
    fotgotPassword: (email: string) => Promise<any>;
}
export class LoginService {

    constructor() {
    }

    async fotgotPassword(email: string) {
        //Axios.post(path,{email_address});
        if (email.length === 0) return Promise.reject({
            status: 1000,
            message: 'Email could not be empty'
        })
        return Promise.resolve({
            status: null,
            message: 'OK',
            data: email
        })
    }



    async saveToken(token: string) {
        const value = await AsyncStorage.getItem("access_token");
        if(value == null){
            await AsyncStorage.setItem("access_token",token);
            
        }
        else{
            //Log token
            AsyncStorage.getItem("access_token").then(res => console.log(res));
        }
    }

    login(info: ILoginParam) {
        
        //Axios find code and password
        //return objectRespone
        var objReponse;

        if(info.code == "admin" && info.password === "123456")
            objReponse = {
                status: 200,
                message: "OK",
                data: { 
                    access_token:" abcxyz123456789",
                    typeUser: "Teacher"
                }
            }
        else
            objReponse = {
                status: 500,
                message: "Failed",
            }
        /////////////
        if(objReponse.status === 200)
        {
            
            this.saveToken(objReponse.data.access_token);
            return Promise.resolve(new LoginDataResponse({
                status: objReponse.status,
                message: objReponse.message,
                data: objReponse.data
            }))
        }
            
        else
            return Promise.reject(new LoginDataResponse({
                status: objReponse.status,
                message: objReponse.message,
            }))
    }
    

}