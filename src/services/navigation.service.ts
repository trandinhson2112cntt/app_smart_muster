import { NavigationActions, NavigationBackActionPayload } from "react-navigation";
import { Alert } from "react-native";

export class NavigationService {
  private static _navigator: any;
  
  static setTopLevelNavigator(navigatorRef: any) {
    this._navigator = navigatorRef;
  }

  static navigate(routeName: string, params?: { [x: string]: any }) {
    try {
      this._navigator.dispatch(NavigationActions.navigate({ routeName, params }))
    } catch (error) {
      Alert.alert(error)
    }
  }
  static back(options?: NavigationBackActionPayload) {
    this._navigator.dispatch(NavigationActions.back(options))
  }
}