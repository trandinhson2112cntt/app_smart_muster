import { IRegisterParam,IRegisterModel } from "../models/register";

interface IRegisterDataRespone{
    status: number,
    message: string,
    data: {}
}

export interface IRegisterService{
    register:(user: IRegisterParam) => Promise<IRegisterDataRespone>;
}
export class RegisterService{
    constructor(){

    }
    register(user: IRegisterModel){
        //axios post user
        if(user.code.length === 0)
            return Promise.reject(
                {
                    status: 500,
                    message: 'Register failed',
                    data: user
                }
            ) 
        else 
            return Promise.resolve(
                {
                    status: 200,
                    message: 'Success',
                    data: user
                }
            )
    }
}