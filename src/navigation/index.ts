import {
  createAppContainer,
  createBottomTabNavigator,
  createSwitchNavigator,
  createStackNavigator
} from 'react-navigation';

import AuthLoadingContainer from '../containers/auth-loading';
import AuthContainer from '../containers/auth';
import HomeContainer from '../containers/home';
import ScheduleContainer from '../containers/schedule';
import StudyContainer from '../containers/study';
import OthersContainer from '../containers/others';

import { Screens } from '../common';
import RegisterContainer from '../containers/register';
import ForgotPasswordContainer from '../containers/forgot-password';
import NotificationContainer from '../containers/notification';
import { colors } from '../styles';
import Calendars from '../components/Calendar';

const homeStack = createStackNavigator({
  Home: HomeContainer,
  Notification: NotificationContainer,
  Calendar: Calendars,
}, {initialRouteName: Screens.Home})

const appStack = createBottomTabNavigator({
  Home: homeStack,
  Schedule: ScheduleContainer,
  Study: StudyContainer,
  Others: OthersContainer
}, {
  initialRouteName: Screens.Home,
  tabBarOptions:{
    showIcon: true,
    showLabel: true,
    activeTintColor: colors.IconTabColor,
    inactiveTintColor: colors.IconTabColorInactive,
  }
})

const authenStack = createStackNavigator({
  Login: AuthContainer,
  Register: RegisterContainer,
  ForgotPassword: ForgotPasswordContainer
}, {
    initialRouteName: Screens.Login
  })


const authStack = createSwitchNavigator({
  AuthLoading: AuthLoadingContainer,
  Auth: authenStack,
  // Register: RegisterContainer,
  App: appStack
}, { initialRouteName: 'AuthLoading' })

export default createAppContainer(authStack)